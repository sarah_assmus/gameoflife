﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameOfLifeManagerOriginal : MonoBehaviour
{
    [SerializeField] GameObject cellPrefab;
    [SerializeField] Transform myPanel;

    [SerializeField] int rows;
    [SerializeField] int columns;

    [SerializeField] Toggle cellToggle;
    [SerializeField] Toggle[] allcellsToggle;


    [SerializeField] float gameTimer = 0f;
    [SerializeField] float timeDelay = 5f;
    [SerializeField] float distance;

    [SerializeField] bool GameisOn = false;



    //TO-DO: 
    // FIGURE OUT HOW TO GET FINDINGMYNEIGHBOURS FUNCTION TO WORK AND RETURN CORRECT NUMBER OF NEIGHBOURS 
    // CREATE MOUSECLICK FUNCTION TO CHANGE STATUS OF TOGGLE isOn BEFORE SIMULATION BEGINS --- done in unity
    // CREATE KEYCODE FUNCTION FOR S TO START SIMULATION, AND ALSO TO PRESS S AT ANY TIME TO PAUSE THE SIMULATION 
    // CREATE A TOGGLE ARRAY HERE, SO THAT IT CAN BE CALLED UPON IN CELL MANAGER TO THEN EVALUATE IF IT LIVES OR DIES 
    // CREATE KEYCODE FUNCTION FOR RIGHT ARROW TO BE ABLE TO ACCESS NEXT ITERATION WHILE PAUSED --> **LILI
    // HAVE THE CELL MANAGER APPLY THE STORED NEXT GENERATION OF VARIABLES TO EACH CELL CREATE TWO FUNCTIONS IN CELL MANAGER....
    // HAVE THE SIMULATION START, WAIT ONE SECOND (1f) AND HAVE IT CONTINUE TO LOOP AND CALCULATE 


    void Start()
    {
        allcellsToggle = new Toggle[1296];
        for (int i = 0; i < rows * columns; i++)
        {
            GameObject mytempGameObject = Instantiate(cellPrefab, myPanel);
            allcellsToggle[i] = mytempGameObject.GetComponent<Toggle>();
            //Debug.Log(allcellsToggle[0]);
        }
        StartCoroutine(WaitThenFindNeighbours());
        /*for testing
                for (int i = 0; i < rows * columns; i++)
                {
                    FindingCellNeighbours(i);
                    //Debug.Log(FindingCellNeighbours(i)); will take a while
                }
        */
        //Debug.Log(FindingCellNeighbours(0).Length);
        //Debug.Log(FindDistanceBetweenCells(0, 1));
        cellToggle.graphic.color = Random.ColorHSV();
    }

    IEnumerator WaitThenFindNeighbours()
    {
        yield return new WaitForSeconds(1);

        for (int i = 0; i < rows * columns; i++)
        {
            allcellsToggle[i].GetComponent<CellManagerOriginal>().SetNeighbours(FindingCellNeighbours(i));
        }

        //FindingCellNeighbours(0);
        //Debug.Log(FindingCellNeighbours(1).Length);
        //Debug.Log(FindDistanceBetweenCells(0, 1));
        //Debug.Log(FindingCellNeighbours(allcellsToggle[0]));
    }
    

    float FindDistanceBetweenCells(int currentCellIndex, int targetCellIndex)
    {
        distance = Vector3.Distance(allcellsToggle[currentCellIndex].transform.localPosition, allcellsToggle[targetCellIndex].transform.localPosition);
        return distance;
    }

    public Toggle[] FindingCellNeighbours(int currentCell)
    {
        List<Toggle> myNeighbourToggles = new List<Toggle>();

        for (int i = 0; i < rows * columns; i++)
        {
            float currentDistance = FindDistanceBetweenCells(currentCell, i);
            if ((currentDistance < 30) && (currentDistance > 0)) myNeighbourToggles.Add(allcellsToggle[i]);
        }
        return myNeighbourToggles.ToArray();

    }


    void Update()
    {
        if (Input.GetKeyDown(KeyCode.N))  //go to other scene.
        {
            SceneManager.LoadScene(1);
        }
        if (Input.GetKeyDown(KeyCode.S))  // this is the code to use the keyboard S
        { GameisOn = !GameisOn;}  //so upon pressing S, is the game is on then it's paused
        if (GameisOn)
        {
            gameTimer += Time.deltaTime;  //timer get increamented 
            if (gameTimer > timeDelay)  // so when the timer is greater than the 1f we set, then the gametimer will reset itself.
            {
                gameTimer = 0f;
                GetCheckedStatus();
                ApplyNewStatusForAll();
                cellPrefab.GetComponent<CellManagerOriginal>().OnLeftClick();
            }
        }
        else //else if game is not on
        {
            while (GameisOn = !GameisOn)
            {
                if (Input.GetKeyDown(KeyCode.RightArrow))
                {
                    GetCheckedStatus();
                    ApplyNewStatusForAll();
                }
            }
        }
        cellPrefab.GetComponent<CellManagerOriginal>().OnLeftClick();
    }

     private void GetCheckedStatus()
    {
        for (int i = 0; i < columns * rows; i++)
        {
            allcellsToggle[i].GetComponent<CellManagerOriginal>().CheckStatus();
        }
    }

     private void ApplyNewStatusForAll()
    {
        for (int i = 0; i < columns * rows; i++)
        {
            allcellsToggle[i].GetComponent<CellManagerOriginal>().ApplyStatus();
        }
    }

}



