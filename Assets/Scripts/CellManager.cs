﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/* Product belonging to Sarah Assmus, Li Li
 * Programming Assignment 2
 */
public class CellManager : MonoBehaviour
{
    [SerializeField] public Toggle mycellToggle;
    [SerializeField] Toggle[] myNeighbours; 
    [SerializeField] bool newCellStatusAlive;  
    Color futureColor;
    Color Orange = Color.Lerp(Color.red, Color.yellow, 0.5f);
    Color clickColor;

    private void Start()
    {
        mycellToggle.graphic.color = Color.green;
        //mycellToggle.graphic.color = Random.ColorHSV(); Just for fun
    }

    public void OnLeftClick()
    {
        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            if (mycellToggle.isOn == true)
            {
                mycellToggle.isOn = false;
            }
            else if (mycellToggle.isOn == false)
            {
                mycellToggle.isOn = true;
                futureColor = Color.green;
            }
            
        }
    }
    public void SetNeighbours(Toggle[] neighbours)
    {
        myNeighbours = neighbours;
    }
    public int NumberOfAliveNeighbours()  //this function is used to count the number of alive neighbours of a cell
    {
        int aliveNeighbours = 0;  //this is the counter to hold the # of the surrounding alive neighbours of a cell
        foreach (var neighbour in myNeighbours)
        {
            if (neighbour.isOn)
            {
                aliveNeighbours++;
            }
        }
        //Debug.Log(aliveNeighbours);
        return aliveNeighbours;
    }

    public int NumberOfRedNeighbours()  
    {
        int redNeighbours = 0;  

        foreach (var neighbour in myNeighbours)
        {
            if (neighbour.graphic.color == Color.red)
            {
                redNeighbours++;
            }
        }
        return redNeighbours;
    }
    public bool RedNeighbourPresent()
    {
        foreach (var neighbour in myNeighbours)
        {
            if (neighbour.graphic.color == Color.red)
            {
                return true;
            }
        }
        return false;
    }

    public void CheckStatus()
    {
        int aliveNeighbours = NumberOfAliveNeighbours();
        if (mycellToggle.isOn && (aliveNeighbours == 2 || aliveNeighbours == 3))
        {
            newCellStatusAlive = true;
            if (mycellToggle.graphic.color == Color.green)
            {
                futureColor = Orange;
            }
            else
            {
                if (mycellToggle.graphic.color == Orange)
                {
                    futureColor = Color.red;
                }
            }
        }
        else if ((!mycellToggle.isOn) && (aliveNeighbours == 3))
        {
             newCellStatusAlive = true;
             futureColor = Color.green;  
        }
        else if (!mycellToggle.isOn && RedNeighbourPresent())
        {
            newCellStatusAlive = true;
            futureColor = Color.green;
        }
        else
        {
            newCellStatusAlive = false;
        }
    }

    public void ApplyStatus()
    {
        mycellToggle.isOn = newCellStatusAlive;  //this assign the boolean value(alive or dead) of the of the cell
        mycellToggle.graphic.color = futureColor;
    }
}