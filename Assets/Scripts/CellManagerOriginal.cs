﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CellManagerOriginal : MonoBehaviour
{
    [SerializeField] Toggle mycellToggle;
    [SerializeField] Toggle[] myNeighbours; //toggle components of the neighbours

    [SerializeField] bool newCellStatusAlive;

    private void Start()
    {
        mycellToggle.graphic.color = Random.ColorHSV();
    }



    public void OnLeftClick()
    {
        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            if (mycellToggle.isOn == true)
            {
                mycellToggle.isOn = false;
            }
            else if (mycellToggle.isOn == false)
            {
                mycellToggle.isOn = true;
                
            }

        }
    }
    //**NEED TO MAKE THIS WORK**
    public void SetNeighbours(Toggle[] neighbours)
    {
        myNeighbours = neighbours;           
    }

    //**NEED TO MAKE THIS WORK**
    public int NumberOfAliveNeighbours()
    {
        int aliveNeighbours = 0;

        foreach (var neighbour in myNeighbours)
        {
            if (neighbour.isOn)
            {
                aliveNeighbours++;
            }
        }
        //Debug.Log(aliveNeighbours);
        return aliveNeighbours;

       
    }
    //TO DO LIST:
    //1. STORE # OF ALIVE NEIGHBOURS IN AN INT VARIABLE (DONE)
    //2. MAKE BOOL FUNCTION TO CHECK CURRENT STATUS OF CELL....IS TOGGLE(isOn) IN THE CURRENT CELL TRUE (ALIVE) OR FALSE? (DEAD) (DONE)
    //2. APPLY RULES LIKE LIVE/DIES/BORN TO CREATE STATUS OF THE CELL: (DONE)
    //  if (aliveNeighbourCount = 2) || (aliveNeighbourCount = 3) (this means EITHER 2 ORRRR 3)
    //        mycellToggle.Toggle.isOn; (or something like this)  (DONE)
    //3. STORE THAT STATUSOF EACH CELL IN A BOOL VARIABLE (TOGGLE - isOn TRUE OR FALSE) (cellNewStatus) (DONE)

    public void CheckStatus()
    {
        int aliveNeighbours = NumberOfAliveNeighbours();
        if (mycellToggle.isOn && (aliveNeighbours == 2 || aliveNeighbours == 3))
        {
            newCellStatusAlive = true;
        }
        else
        {
            if (aliveNeighbours == 3)
            {
                newCellStatusAlive = true;
            }
            else
            {
                newCellStatusAlive = false;
            }
        }
    }

    public void ApplyStatus()
    {
        mycellToggle.isOn = newCellStatusAlive;
    }
}
