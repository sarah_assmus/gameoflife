﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

/* Product belonging to Sarah Assmus, Li Li
 * Programming Assignment 2
 */
public class GameOfLifeManager : MonoBehaviour
{
    [SerializeField] GameObject cellPrefab;  
    [SerializeField] Transform myPanel;  

    [SerializeField] int rows;
    [SerializeField] int columns;

    [SerializeField] Toggle[] allcellsToggle;

    [SerializeField] float gameTimer = 0f;
    [SerializeField] float timeDelay = 5f;
    [SerializeField] float distance;
    
    [SerializeField] bool GameisOn = false;

    void Start()
    {
        allcellsToggle = new Toggle[1296];
        for (int i = 0; i < rows * columns; i++)
        {
            GameObject mytempGameObject = Instantiate(cellPrefab, myPanel);
            allcellsToggle[i] = mytempGameObject.GetComponent<Toggle>();

        }
        StartCoroutine(WaitThenFindNeighbours());

    }
    IEnumerator WaitThenFindNeighbours()   
    {
        yield return new WaitForSeconds(1);
        for (int i = 0; i < rows * columns; i++)
        {
            allcellsToggle[i].GetComponent<CellManager>().SetNeighbours(FindingCellNeighbours(i));
        }
    }
    
    float FindDistanceBetweenCells(int currentCellIndex, int targetCellIndex)
    {
        distance = Vector3.Distance(allcellsToggle[currentCellIndex].transform.localPosition, allcellsToggle[targetCellIndex].transform.localPosition);
        return distance;
    }

    public Toggle[] FindingCellNeighbours(int currentCell)  
    {
        List<Toggle> myNeighbourToggles = new List<Toggle>();
        for (int i = 0; i < rows * columns; i++)
        {
            float currentDistance = FindDistanceBetweenCells(currentCell, i);
            if ((currentDistance < 30) && (currentDistance > 0)) myNeighbourToggles.Add(allcellsToggle[i]);
        }
        return myNeighbourToggles.ToArray();
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.N))  //go to other scene.
        {
            SceneManager.LoadScene(0);
        }
        if (Input.GetKeyDown(KeyCode.S))
        { GameisOn = !GameisOn; }
        if (GameisOn)
        {
            gameTimer += Time.deltaTime;  
            if (gameTimer > timeDelay) 
            {
                gameTimer = 0f;
                GetCheckedStatus();
                ApplyNewStatusForAll();
                cellPrefab.GetComponent<CellManager>().OnLeftClick();
            }
        }
        else 
        {
            while (GameisOn = !GameisOn)  
            {                              
                if (Input.GetKeyDown(KeyCode.RightArrow))  
                {
                    GetCheckedStatus(); 
                    ApplyNewStatusForAll();


                }
            }
        }

        cellPrefab.GetComponent<CellManager>().OnLeftClick();
    }
     private void GetCheckedStatus()
    {
        for (int i = 0; i < columns * rows; i++)
        {
            allcellsToggle[i].GetComponent<CellManager>().CheckStatus();
        }
    }
    private void ApplyNewStatusForAll()  
    {
        for (int i = 0; i < columns * rows; i++)
        {
            allcellsToggle[i].GetComponent<CellManager>().ApplyStatus();
        }
    }

    /*private void ApplyGreenFunction()
    {
        for (int i = 0; i < columns * rows; i++)
        {
            allcellsToggle[i].GetComponent<CellManager>().ApplyGreenColor();
        }
    }
    */


}



